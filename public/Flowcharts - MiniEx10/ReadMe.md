### A FLOWCHART
A flowchart is a type of diagram that represents a workflow or process. A flowchart can also be defined as a diagrammatic representation of an algorithm, a step-by-step approach to solving a task. Conventionally, each step in a flowchart is represented by a symbol and connecting lines that guide the flow of logic progressively towards a certain output. The symbols that are used are shapes and each one carries a different meaning.

![Screenshot](FlowchartShapes.png) 

* Oval: Indicates the start or end point of a program/system.
* Rectangle: Represents the processual steps.
* Diamond: Indicates the decision points with yes and no branches.

<!-- blank line -->
----
<!-- blank line -->

### INDIVIDUAL
I have chosen to create a flowchart based on my 4th MiniEx:

**RunMe:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx4/

**Sketh.js:** https://gitlab.com/laurakirstineschulz/ap2020/-/blob/master/public/MiniEx4/sketch.js


In this week's individual task, we were asked to create a flowchart based on our most “technically complex” MiniEx. When I look back at my MinExes, with the knowledge I have today, this may not be the most technically one. However, I remember this being the exercise I had the hardest time programming at the moment it was made. It was also the first time I used defined variables, DOM elements as well as a for-loop. That's why I chose this MiniEx as my most technically complex one.

![Screenshot](MiniEx4.png) 

<!-- blank line -->
----
<!-- blank line -->

### AS A GROUP
To be honest, before this week’s start, we had no idea what we wanted our final program to be. Nevertheless, we got talking and found two possible ideas:

**1. Do I have Corona?**

The first idea we came up with originated from what we imagine many people around the world have been thinking lately: Do I have corona. As our flowchart (hopefully) shows, this program is meant to be a kind of test. Each decision box represents one question. The idea is that that question will appear on canvas. Along with the question, there will also be two buttons: yes or no. By clicking one of these buttons, the next question will appear. However, the question that appears depends on the past answer. Thus, one is guided through a Do-I-have-Corona test, based on the answers they are giving the program.

![Screenshot](CoronaTest.png) 

One of the technical challenges in making this program is the many if-statements it must contain in order for the right questions to display on canvas. Logically, it will take a long time. Another challenge is to make sure that the layout is aesthetic, neat and have a connection to the theme: Corona.

<!-- blank line -->
----
<!-- blank line -->

**2. Does Corona have an impact on the environment?**

The second idea we came up with is based on an API containing the numbers of particles and heavy metals in the air (pollution) along with an API containing the numbers of corona infected. Scientists believe that this pandemic has had a good impact on the environment, and therefore we are now seeing a reduction in pollution. This claim, we wanted our program to prove. When opening our program, the user will have the opportunity to click on a number of countries, after which the user can compare the number of corona infected with the number of air pollution. 

![Screenshot](Pollution.png) 

However, after making this flowchart, we were not completely satisfied. If we choose to move forward with this idea, we would also like our program to show the pollution before the corona-virus was upon us, meaning that the user has something to compare the current numbers with. This information could possibly be found in a json-file. In the future, our program would therefore not consist of two bars, but three: One showing the number of corona cases, one showing the air pollution before corona, and one showing the current air pollution.

The challenges of this program are obviously finding the right IPAs. This program seems more technically challenging than the one above. Furthermore, we need to navigate two different APIs as well as a possible JSON file. Also, we want to display certain information in connection to certain countries. This will be a challenge as well. 

<!-- blank line -->
----
<!-- blank line -->

### README
**DIFFERENCES, CHALLENGES AND VALUES OF FLOWCHATS**

All of the flowcharts above show a different way of constructing a flowchart.

The flowchart made in connection with my MiniEx 4 differs from the two others by being made after the program was finished. This meant that I was able to go more into details with the technical aspects. Nevertheless, a challenge could be that you must be familiar with programming language in order to understand this flowchart. I think that this flowchart comes out looking more finished, but is it understandable for everyone? I think not. The flowchart for MiniEx 4 is seen from the programmer's point of view. 
On the other hand, the two flowcharts made in collaboration with my study group are being looked upon from the user's point of view. They serve as drafts to how our final project could be looking like. They are less technical and build in a simpler way that everyone should be able to understand. A challenge with this way of making a flowchart could be that the programer finds it hard to visualize the full idea.
This suggests that to whom the flowchart is intended, has an impact on how the idea is represented. One could
argue that flowcharts are a useful and values way of showing solutions to problems even, even when they are complex.


**ALGORITHYMS AND FLORCHARTS**

When thinking about algorithms, people often have different descriptions of what they are and what they are used for.  According to dictonary.com are algorithms “a set of rules for solving a problem in a finite number of steps, as for finding the greatest common divisor.” However, Tania Bucher claims that algorithms can be seen as both "magical and concrete, good and bad, technical and social", meaning that algorithms exist on many scales ranging from how software operates on a technical level, to how it impacts society.

Algorithms and flowcharts can both be seen as recipes for instructions to be executed and rules to be followed which is why one can say they are deeply intertwined with each other. They both strive for giving the most organized visualization possible of the tasks to be conducted, while placed in the most efficient order. However, flowcharts are primarily used as a medium for communication between humans. It is a way of grasping the complexity of algorithms. So, flowcharts are the comprehensible way for humans to understand algorithms.
> “From a technical standpoint, creating an algorithm is about breaking the problem down as efficiently as possible, which implies a careful planning of the steps to be taken and their sequence.”  (Bucher 2018, p. 6)

As suggested here, the creation of algorithms is done very thoughtfully with the goal of obtaining the wanted results in the most efficient way, and this exact process is quite similar when wanting to make flowcharts. You create a path under a specific set of rules with the aim of achieving a wanted outcome. Similar processes can be seen in social and cultural contexts, where specific rules, as well as unspecified rules, must be followed. Through governments and politics, all societies are basically built upon algorithms with various rules and laws to be obeyed, tasks in the form of labor and education to maintain the social- and economic infrastructure. They are designed to make everything run smoothly and generate a good life for its citizens. According to mathematician professor Marcus du Sauloy, algorithms have taken over modern life without us noticing. 


**SOURCES**
* Dictionary: https://www.dictionary.com/browse/algorithm
* Text: Bucher, Taina. If...THEN: Algorithmic Power and Politics, Oxford University Press, 2018, pp. 19-40
* Video: Marcus du Sautoy, Algorithms - The Secret Rules of Modern Living - BBC documentary
