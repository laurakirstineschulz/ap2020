function setup() {

//Filling in the whole canvas
createCanvas(windowWidth, windowHeight);

//Setting the backgrond
background(153, 153, 102);

}

function draw() {

//Increases the size of a emoji
  scale(6);

//Making the ellipse of the whole emoji
  fill(223, 190, 159);
  noStroke();
  ellipse(75,75,50,50);

//Eyes
  fill(255);
  stroke(198, 138, 83);
  strokeWeight(0.7);
  ellipse(66,70,10,14);
  ellipse(84,70,10,14);
  fill(51,26,0);
  ellipse(66,73,8,8);
  ellipse(84,73,8,8);

//Happy mouth
  noFill();
  strokeWeight(1);
  arc(75, 75.8, 26, 26, 	QUARTER_PI,PI-QUARTER_PI);

if(mouseIsPressed) {

//Making the ellipse of the 2'nd emoji
  fill(153, 102, 51);
  noStroke();
  ellipse(75,75,50,50);

//Eyes of the 2'nd emoji
  fill(255);
  stroke(96, 64, 31);
  strokeWeight(0.7);
  ellipse(66,70,10,14);
  ellipse(84,70,10,14);
  fill(51,26,0);
  ellipse(66,73,8,8);
  ellipse(84,73,8,8);

//Sad mouth
  noFill();
  strokeWeight(1);
  arc(75, 98, 30, 26, 	PI+QUARTER_PI,2*PI-QUARTER_PI);

}
}
