**LINK:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx2/
- Please pay attention to if (mouseIsPressed).


**What have I used, and what have I learnt**

I used variables to create my canvas: createCanvas (windowWidth, windowHeight). This syntax is used when wanting to fill the entire canvas in any web browser, running the code.

I then began to code what I associate with a classic smiley-emoji. I did this by using ellipses. Here I trained the use of shapes, but I also worked with the placement of the ellipses on my X and Y axis. Also worked with the syntax of "fill" and “stroke". I had a hard time predicting the colors (RGB), but using the following website: https://www.w3schools.com/colors/colors_picker.asp I quickly found that I could easily see the colors, the different shades, and the corresponding color codes within the RGB. I struggled with making the emoji's mouth, but using p5.js' reference list, I found the “arc”. Also I found a YouTube video explaining the arc, and finally I succeeded.

I wanted my emoji to be two-sided: a happy emoji, in a light complexion (when clicking it) transforming into a sad emoji with a darker complexion. Why so, I will explain in the section below. To achieve this "change" of my emoji, I used another variable, namely: if (mouseIsPressed). After entering this, I copy passed my first emoji. Then I changed the color codes as well as the arc, now to be downward.

At last i found that while doing my programing, i had zoomed in on my webbrowser. This meant that my emojis were very small. That's why I used "scale" to magnify it. I learned about scale through the reference list of p5.js.


**How to put my emojis into a wider cultural context:**

In 2015 Apple introduced its new racially diverse emoji, allowing users to cycle through various shades of white and brown to customize their emojis skin colors. For some, this new opportunity was extremely well received. But in trying to advocate for racial inclusivity in its iOS 8.3 update, Apple has allowed for further racial segregation with these new emoji. 

Basically, the standard emoji on Apples emoji-interface is with a light complexion. But if one is black, should they now feel compelled to use the “appropriate” brown-skinned emoji? I find, that what Apple has done is introduce race into everyday conversations where it doesn’t necessarily need to be. Apple’s intent was good. But the execution was completely flawed. Apple took the easy way out. Instead of creating actual emojs of color, Apple simply allows its users to make white emoji a different color. With this update, the company skirts around having to attribute certain physical characteristics to certain races of people. For example, there’s nothing specifically “black” about an emoji with browner skin. Deepening the skin color of a previously white emoji doesn’t make the emoji not white. It’s just a bastardized emoji blackface. The blond-haired emoji man and the blue-eyed emoji princess are clearly white, but you can slip them into a darker-colored skin. These new figures aren’t emoji of color; they’re just white emoji wearing masks.

My emojis are very simple, with no character traits. The dark emoji has a downward mouth, as a comment on the above.
