//The variables I will use in order to set the colors later on
let bgColour;
let storkeColour;

let step = 30; //The distance between each lines, and the step that the loop changes its variable by each time it’s run


function setup() {
createCanvas(windowWidth, windowHeight); //Filling in the whole canvas
frameRate(10); //The number of frames to be displayed per second

//Defining my variables
bgColour = ('olive'); //CSS Colour
storkeColour = ('cornsilk'); //CSS Colour
}


function draw() {
//Setting the background and the lines to the colours of the variables
background(bgColour);
stroke(storkeColour);

//The lines that go across the x-axis / down the page y-axis
for (let x=0;x<width;x=x+step) {
for (let y=0;y<height;y=y+step) {
 strokeWeight(5);

//Command and draw lines if the value is bigger than 0.5 (half the time)
if(random()>0.5) {
 line(x,y,x+step,y+step); }
//Otherwise:
else {
 line(x+step,y,x,y+step); }
}
}
}

//If mouse is pressed, the progam will change its colour
function mousePressed() {
bgColour = ('tan'); //CSS Colour
storkeColour = ('gold'); //CSS Colour
}

//If mouse is release the colours will go back to it's original
function mouseReleased() {
bgColour = ('olive'); //CSS Colour
storkeColour = ('cornsilk'); //CSS Colour
}
