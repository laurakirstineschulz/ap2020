**RUNME:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx7/

**SCREENSHOT:** Please note that my screenshot is attached to this folder.



**THE RULES OF MY PROGRAM**
1. My program must be constantly moving, without any interaction.
2. My program must be two-dimensional in terms of changing color palette.

When opening my program you will find a canvas filled with random lines. All of these lines are moving within the same ‘frameRate’ and together, these lines form a moving pattern. When pressing your mouse, the same pattern will appear but the colors will have changed.

In order to fulfill rule number 1, I used a ‘for loop’. A ‘for loop’ creates a loop that is useful for executing one section of code multiple times. I combined this with a conditional statement, also using p5.js’ random function, and suddenly I was able to get my lines to constantly reposition on the canvas. To meet rule number 2, I defined two variables to have a specific color: the background color, and the color of the strokes/lines. By putting in ‘function mousePressed’ followed by the variables and a new color code, I was able to change my whole color palette for my program. Since I am working in a program that has access to quite a few named colors (CSS colors) I found inspiration on this website: https://colours.neilorangepeel.com in order to make my choice of color. To finish this off, I used ‘function mouseReleased’ to put my first selected colors back in my program. Also, I set the ‘frameRate’ = 10. If I did not do this my program would run 60 times pr. second and therefore the pattern would change extremely fast. I have added quit a few notes in my sketh.js so my approach should also be easily read from there.

Processing rules is a type of rule that does not specify any specific remedies as resolution of specific situations. Instead, they specify the rule of reasoning or processing. Although rule based processing rules can be extremely useful in the design reasoning process, one must not forget that the choice or decision about the solution is subjective by nature and there is no absolutely objective criterion. Therefore, I see my set of rules as a guideline with hundreds of different results, and not like a mathematical formula with only one certain result.



**GENERATIVE ART**
Part of the required reading for this week was a video called “Beautiful rules: Generative models of creativity”. In this video, I came across a quote by the artist Marius Watz, who says that: “*Today most of the time we don't control the software, the software shapes how we work creatively.*” 
This is a statement that I highly agree with, and something that I have experienced quit a few times during my mini ex’s. I find this interesting when talking about creating software art based on a set of rules. I guess before starting this course, I had a preconception that software was not a material that allowed a lot of experimentation. Also I had a preconception that you were supposed to have a clear idea of the end result before being able to code something at all. But now I know that using software as a material for art enables one to create things that would not be possible in any other medium. Also, when talking about the rules of programming: there is not only one result - there are hundred of different results because the software shapes us individually.

The use of functions such as random(); enables one to create artworks with autonomous behaviour without knowing the end result. As the chapter “Randomness” by Nick Montford et al. touches upon, the concept of randomness is something that we have always been intrigued by, and by giving autonomy to the computer and allowing it to decide, this can create art.



**SOURCES**

*  Marius Watz, "Beautiful Rules: Generative Models of Creativity", in The Olhares de Outono (2007), https://vimeo.com/26594644.
*  Montfort, N, et al. "Randomness". 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, The MIT Press, 2012, pp. 119-146
