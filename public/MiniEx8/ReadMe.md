**This Mini Ex is made in collaboration with Line Stampe-Degn Møller.**

**RUNME:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx8

**SCREENSHOT:** Please note that the screenshot is attached to this folder.

**A BOOK OF RECIPES**
With this mini exercise we have programmed a virtual cookbook. The virtual book contains 9 different dishes. By browsing the book, you can see both the ingredients each dish requires as well as the steps you need to go through in order to make the current dish. When wanting to flip a page to see the next dish, please press the paper fold at the bottom of the book. You can browse the pages both back and forth by using the paper-fold-buttons.

If looking at the source code you will find yet another recipe. This is a recipe for making a code. Just like a regular recipe (the ones displayed at the virtual book) you will see an ingredient list. This list contains the variables of the code. In the source code you will also find steps that indicate how this “dish” should be put together. Of course you will also be guided in seasoning your code with text as well as arranging it using CSS styling.

**OUR PROGRAM**
In order to make this program we have used an already existing JSON file: https://github.com/raywenderlich/recipes/blob/master/Recipes.json. By using JSON, all the data can be upload in this file without changing anything in the actual source code. As you see from our sketh.js, we have called loadJSON() inside preload(). By doing this JSON guarantees to complete the operation before setup() are called. Therefore setup() will wait until our JSON file has finished loading. Next up we created two buttons: One to flip a page forward and one to flip a page backward. We did this by inserting images of paper folds and then using `buttonLeft = createImg`. By using conditional statements, we were able to code our program to “show” or “hide” the buttons. This meant that we could control that one could not get any further than page/recipe 1-9. In our preload() we defined `coding = loadJSON("recipe.json")`. By doing this, we could now retrieve our JSON file into our source code using
```
for(var k = 0; k < coding[is].writtingARecipe.length; k++) {
text(coding[is].writtingARecipe[k].toCook, 170, k*17+200) }
```
The above displays the ingredients. By copy pasting this, replacing toCook with forMachines, and changing a few other things (see sketh.js) we could also display steps. Next we created the headlines and page numbers using text. Also, we did a bit of CSS-styling throughout our entire program. At the very last we made  //notes in our source code. We did this with the purpose of displaying the code like a food recipe. Please check this out in our sketh.js.

**REFLECTIONS**
Programs are very similar to recipes. They both give instructions that, if followed, achieve something. Though, there is a difference between them and it has to do with language. When chefs invent recipes they write them out in human languages like Danish or English. Programmers write programs in other languages, but knowing the language one can speak it.

The first thing to notice about our recipe book, and recipe books in general, is there is a clear structure. Each recipe is obviously separate from the others. Each has a title, an ingredients list and then a series of steps to follow. Programs follow a similar structure - and this is what we aim to show in our source code.

The following quote is for this week readings: Speaking Code:
“*This chapter aims to extend this discussion about the relation between spoken language and program code, but in particular to speculate on vocable code and the ways in which the body is implicated in coding practices. Through the connection to speech, programs can be called into action as utterances that have wider political resonance; they express themselves as if
possessing a voice.*” 
Not only does this quote sum up this week's readings, but it can also be translated in relation to our Mini Exercise. It does so by addressing how programs can “express themselves”. You will find that our program expresses itself through the instructions in the source code. Furthermore, the quote above articulates “the relation between spoken language and program code”. As mentioned above, a code could in fact be seen as a spoken language.

In order to analyse the use of JASON, the text called Speaking Code mentions the Dutch systems scientist, programmer, software engineer, Edsger Dijkstra. Dijkstra says: “*whereas machines must be able to execute programs (without understanding them), people must be able to understand them (without executing them)*”. We, as human beings, are able to understand a JSON-file because we are aware of its content. The computer, on the other hand, does not understand the content, but still it is able to execute the files. This is a great example of how humans and machines are very different from each other.  
 
At last, the theme of recipes can be related to last week's mini exercise in the sense that rules can be like a recipe. They tend to be effective when it comes to using known solutions to address situations based on experiences - just like programming.

**SOURCES**
* Geoff Cox, and Alex McLean. Speaking Code. Cambridge, Mass.: MIT Press, 2013. 17-38.(The chapter Vocable Code) 