**LINK:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx5/

**SCREENSHOT:** Please note that my screenshot is attached to this folder. It is called “MiniEx5.png”.

This program is mainly inspired by my first mini exercise: https://laurakirstineschulz.gitlab.io/ap2020/MyFirstSketch/


**MY FIRST VISION**
My vision with this mini ex was to redesign my very first program. My first program consisted of a 3-dimensional dice which I made using squares, quads and ellipses. I wanted to redesign my program so that the dice was able to be tossed and the toss itself would be displayed on screen.
However, that did not happen…
While trying to reach this vision, I first of all realized I was not skilled enough to do this. Second, I figured that I wanted my program to contain a little of what I have learned during all of my mini exercises.


**THE ACTUAL IDEA**
Even though I did not go along with my first idea, I still decided that my first mini ex should be my departure point. I have in my present program used the same color range as I used in my mini ex 1. Also, my concept of this program is based on the dice from my first mini ex: The idea was to make a game. When rolling the dice a playbrik would move around on a game board. 

The dice seen in the left corner is actually the dice from my mini ex 1. I took a screenshot of the one I made back then. I then created a button, and placed the image as the buttons interface. I could not get it to move the way I wanted to. But instead I programmed, that when clicking dice, the tekst next to it will show a random number from 1 to 6 - just like a regular dice. The playbrik will move the same number as the dice shows. This action will loop, and therefore one can keep on “throwing” the dice, and moving the brik around on the play board. In my sketch.js  I included //notes every step of the way, so my approach to making this program should be easily read from there.

I am not one hundred percent satisfied with my program. I think it lacks some ethics. In addition, I would have liked to make each of the fields, on the play board, consist of some kind of happening. Unfortunately, I ran out of time.


**WHAT I HAVE USED FROM MY PREVIOUS MINI EXES**
* Mini ex 1: As writing above, I made use of the color range and concept of a dice.
* Mini ex 2: In mini ex 3 I practiced shapes. In my present program I have eg. used the ellipse.
* Mini ex 3: In this mini ex I trained how to get objects to move circles. Therefore it was a conscious choice for my game board to be round, and the playbrik to move in a circle. I thought of it as a sort of an interactive throbber.
* Mini ex 4: In the mini ex I made for this week, worked quite a lot with buttons. I found out how useful “if mousePressed” is, as well as how many different ways it can be used. I took this experience with me as I made my mini ex 5.



**REFLECTION ON AESTHETIC PROGRAMING**
Aesthetic programming is the connection between society and coding. It is in the alternation between levels of abstraction, going from looking at specific syntax to looking at how different codes and the ideas affect our society. Therefore, aesthetic programming makes it clear that programming is not a neutral action – there are thoughts and ideologies behind every choice that is made, and these choices have an impact on culture.
