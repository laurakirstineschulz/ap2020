let di;
let di_2;
let oldNum = 1;
let diceNumber = "Roll the dice";
let newNum;
let button;
let roundAreas;
let start;


function setup() {
 createCanvas(650, 650);

//Creating the dice-button in the left corner
 button = createImg('Button_MiniEx1.png');
 button.size(80, 80);
 button.position(10, 560);
 button.mousePressed(roll);
 di = width/ (1 + (1/4));
 di_2 = width/2;
}


function draw() {
 background('hsb(190, 50%, 50%)');
 board();
}

//The white board in which the brik moves
function board() {
 noStroke();
 fill(255);
 ellipse(width/2, height/2, di);
 if (oldNum < newNum){
 oldNum ++;
 if(oldNum-start >=(12*roundAreas)){
}
}

//The black brik
 fill(0, 0, 0, 100);
 let xCordi = (width/2 + (di_2/1.55) * Math.cos(2 * oldNum * (Math.PI / 12) + (Math.PI / 12)));{
 let yCordi = (height/2 + (di_2/1.55) * Math.sin(2 * oldNum * (Math.PI / 12) + (Math.PI / 12)));
 ellipse(xCordi, yCordi, 50, 50);
 fill(0);
 textSize(13);
 text("START", 453,480);
}

//The lines within the white board
 stroke(0);
 for(i = 0; i < 12; i++){
 xVal = (width/2 + (di/2) * Math.cos(2 * Math.PI * i / 12));
 yVal = (height/2 + (di/2) * Math.sin(2 * Math.PI * i / 12));
 line(width/2, height/2, xVal, yVal);
}

//The inner ellipse
 fill('hsb(190, 50%, 50%)');{
 noStroke();
 ellipse(width/2, height/2, di_2);
}

//The text in the middel showing the dice number
 fill(0);{
 textSize(35);
 text(diceNumber + "", 95, 625);
}
}

//Making the the dice number random
function roll(){
 diceNumber = Math.floor((Math.random() * 6) + 1);
 newNum = oldNum + diceNumber;
}
