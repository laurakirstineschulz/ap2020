**LINK:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx6/


**SCREENSHOT:** Please note that my screenshot is attached to this folder.


I have been quit sick this week, and in that context, I’ve been quit afraid of dying. The god news is that I literally just had a regular flu. The bad news is that I haven't had much time to make program. Of course, that is regrettable.




**MY PROGRAM**
This program is inspired by a p5.js example: https://p5js.org/examples/hello-p5-interactivity-1.html

Before I started the coding og this game I needed to create some variables to hold my values, such as the circle’s, radius, x and y coordinates, and colors. As you might notice, I used both *const* and *let*. *Const* is used for variables that won’t change, and *let* is used for variables that might change. Since my circle is moving randomly around the screen, its x and y coordinates will definitely change, and therefor I used let. On the other side, I knew that I wanted my radius to stay = 100. Therefor I used const.

After setting my values I moved on to *function setup*. Within the setup I made my game take up the entire window by setting `createCanvas(windowWidth, windowHeight)`. Below that, I defined my circle’s first x, y location. By using `random(windowWidth) / (windowHeight)`, I was able to place the circle at a random x and y-coordinate between 0 and the width and hight. I also defined the color range I was going to use in my program.  I knew that I wanted only warm colors within reds and green. Therefor I used the values I did, and made the blue tone = 0. In here, I used the random function as well, in order to make the colors change.

Now, moving on to *function draw* I was able to add my cirkel. Inspired by the program I linked at the top of this ReadMe I created a cirkel using:  *strokeWeight(2), stroke(r, g, b), fill(r, g, b, 100)*, and finally the `ellipse(x, y, radius*2, radius*2)` to add the cirkel to the screen, at its pre-determined random x and y location.

I wanted my program to respond to the users clicking their mouse, so the program would give them points if clicking on the circle. In order to do that I added my third function into my codes: *mousePressed()*. Inside this function I wanted to check if the user had clicked on the circle. First, I’ll calculate the distance between the x, y location of the mouse and the x, y location of the circle. I did this with the help of `let d = dist(mouseX, mouseY, x, y)`. If the distance (stored in d) is less than the radius of the circle, the program knows that the user has clicked inside the circle.  By using `if (d < radius)` I made a conditional to do something if the distance is less than the radius: Namely, to count points. At the top of my code, I added in another variable to keep score. I initialize it to 0 so that the program would start off at 0 points. I addition I wanted my score / points to by displayed on the screen. To do this I simply added some text within the draw function: `text (“Points: " + score, 40, 60)`.

To make my game more challenging, I wanted to continuously relocate the circle to a new position. Therefore I made a new function called *newCircle*, and positioned it randomly. Finally, at the very bottom of my code, I used a JavaScript tool called *setInterval* to relocate the circle in a certain speed.

At the very end I had a vison of making my game harder when the user reached a certain number of points - I wanted the circle to get smaller when the user racked up 10 points. I tried this code:
`
if (score == 10) {
radius /= 2;
}
`
But it did not work. Any sugestins?




**OBJECT ORIENTED PROGRAMING**
Object-oriented programming refers to a type of computer programming in which programmers define the data type of a data structure, and also the types of operations that can be applied to the data structure. One of the key concepts Object-Oriented Programming is abstraction. The main goal when talking about abstraction is to select only the most important aspects of a larger pool of data to show in order to reduce complexity. The choice about abstraction has a great impact on the final product, and how it is perceived. This supports the claim from an earlier Mini Ex that programming is not a neutral act.



