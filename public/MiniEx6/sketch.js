let score = 0; //Let is used for variables that might change
const radius = 100; //Const is used for variables that won’t change
let x;
let y;
let r;
let g;
let b;

function setup() {
createCanvas(windowWidth, windowHeight);

 //Defining the circles first x and y location:
 x = random(windowWidth);
 y = random(windowHeight);

 r = random(100, 225);
 g = random(100, 200);
 b = random(0);
}


function draw() {
background(255);

 //Drawing a circle
 strokeWeight(2);
 stroke(r, g, b);
 fill(r, g, b, 100);
 ellipse(x, y, radius*2, radius*2);

 //Maiking the score-munber in the upper left corner
 textSize(30);
 noStroke();
 fill(r, g, b);
 text("Points: " + score, 40, 60);
}


function newCircle() {
 x = random(windowWidth);
 y = random(windowHeight);
 r = random(100, 225);
 g = random(100, 200);
 b = random(0);
}


function mousePressed() {
 // Checking if the mouse is inside the circle
 let d = dist(mouseX, mouseY, x, y);
 //Adding one point, every time the cirkels is pressed
 if (d < radius) {
 newCircle();
 score++;
}}


//Setting the speed of the cirkels movement
setInterval(newCircle, 800);
