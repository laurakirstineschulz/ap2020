I will describe my first experience of coding surprisingly fun. There are still many aspects I do not understand, but I have already gained a much better understanding when it comes to simple things such as shapes and colors. I have benefited greatly from the reference list of p5.js website, and a series of youtube videos.

I find that both coding and writing aim to get a message out, in a way that is easy to understand. As we write, it is important that the reader understands it. I think the same is applies when we code. The codes must be understandable so that a machine can read it; It must accurately tell a computer, website or application precisely what it needs to do.

By reading the texts for this week, I gained a much better understanding of why programming is an important lesson. Specifically, I found the comparison of not being able to program as a digital designer is equivalent to a carpenter being unable to put a drill into a drilling machine, quite interesting; As a digital designer, you will be much more limited in terms of what actions / designs you can do if you are not familiar with programming.

MY FIRST SKETCH: https://laurakirstineschulz.gitlab.io/ap2020/MyFirstSketch/

SCREENSHOT: 
