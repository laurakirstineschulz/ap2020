**LINK:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx4/


**SCREENSHOT:** Please note that my screenshot is attached to this folder.




**I GUESS THIS IS YOU?**
I have developed a sketch in response to the transmediale festival (2015) within the theme CAPTURE ALL. My sketch is called I GUESS THIS IS YOU? and it focuses on capturing and processing input data. It does so with the help of the JavaScript library called p5.js, in which I have used to make my artwork.




**MY PROGRAM**
The first thing you see, when using my program, is a white canvas. In the left corner of the canvas you will see an input bar, a ‘submit’ button and a greeting asking "Who are you?” The intention is that you have to enter your name in the input bar and press the 'submit' button. Now your webcam will be turned on and a live image of yourself will appear. At the same time, your name (the one you wrote in the input bar) will fill the canvas in a random order. Furthermore, the greeting will now have changed to "I Guess this is you then?”. You can continue to enter words or phrases in the input bar. The words/phrases you enter will also appear on the canvas, forming a form of collage. If you want to clear the canvas, simply press the enter button on your keyboard.

In order to make my program, I found a lot of inspiration in some of p5.js' own work: https://editor.p5js.org/p5/sketches/Dom:_Input_Button.

When looking at the syntaxes that I have used, I started off with creating names for the variables I wanted my program to contain. I did this by using the *let* function.
In my *function setup*, I listed everything I wanted to be there throughout the entire program, ie everything that should run in a loop. That includes: *createCanvas, createInput, createButton, greeting = createElement*. Then I proceeded to *function drawElement*. I used the declaration called *const* in order to make the headline change once pressing the button. *Const* creates and names a new constant. In this case, the variable: *greeting* created with *let* is the container for the value created with *const*. In my *drawElement* I also added a video function that uses the webcam to create a live video. In order to make whatever is typed in the input bar appear on the canvas I used a syntax inspired by the program in the link above. At last I added *keyPressed (clear)*. I did this in the purpose of making a function that would wipe the canvas clean.




**CAPTURE ALL**
We may not notice it, but our data is constantly being captured when we use our computers. But how is this done? In fact, we are the ones to blame.
Data is most often captured through our interaction. An example of this is the like-button on facebook. I once read that the like-button is being pressed more than 1,8 millions times every minute and since: *“The implementation of Like buttons enables data flows between the platform and external websites that enter multiple processes of exchange and contributes to a simultaneous de- and recentralisation of the web, advancing Facebook as the central hub.”* (Gerlitz and Helmond, first page) It is clear that our data floats all over the internet.

But we do have a desire to press that button. This is not just a desire to press the like-button, but any button. I mean, how many times have you accepted terms and conditions, without reading the details? You search for that button, right? But when you press that button, you also give your permission for the web to capture your data.

In my program you will be asked to press the 'submit' button. When doing that, the program captures the information: “who are you?” (At least if you answered the question truthfully). Now you will be led to the second button, which allows your webcam to be used. I guess you just pressed yes without reading the terms? My program has now captured both your definition of you, and your face. All of the above in an attempt to CAPTURE ALL.

*  Carolin Gerlitz and Anne Helmond, "The Like Economy: Social Buttons and the Data-Intensive Web", New Media & Society 15: 8 (December 1, 2013): 1348–65
