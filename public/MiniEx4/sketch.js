let input;
let button;
let greeting;
let capture;


  function setup() {

//Filling in the whole canvas
  createCanvas(windowWidth, windowHeight);

//Creating the input line
  input = createInput();
  input.position(150, 150);

//Creating the text in the left corner of the canvas
  greeting = createElement('h2', 'WHO ARE YOU?');
  greeting.position(150, 90);

//Creating the "submit" button
  button = createButton('submit');
  button.position(300, 150);
  button.mousePressed(drawElement); //When mouse pressed go to the funtion drawElement()
}

//The following will appear if the submit button is pressed:
  function drawElement() {

//Setting the greeting text to chance
  greeting.html('I GUESS THIS IS YOU?');

//Programing the webcam to turn on
  capture = createCapture(VIDEO);{
  capture.size(500, 500);
  capture.position(width/3, height/4);

// Making whatever is typed in the input line appear on the entire canvasas
  const name = input.value();

  for (let i = 0; i < 50; i++) {

  push();
  translate(random(width), random(height));
  fill(0, 80);
  textSize(50);
  textFont('Georgia'); //CSS styling
  text(name, 0, 0);
  pop();

}}}

//Automatically adjusts the height and width whenever the size of the window is increased
  function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

//Restarting the canvas when enter is pressed
  function keyPressed() {
  if (keyCode === 13) //Enter= 13
  clear();
}
