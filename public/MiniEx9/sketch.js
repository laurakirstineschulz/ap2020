// Button variables
let buttonNegative;
let buttonPositive;

// Url, API and search words
let api = "https://pixabay.com/api/?key";
let apiKey = "=15864277-2663e5dcc5f0f59a5c1528f4c";
let q = ["&q=depression", "&q=virus", "&q=death", "&q=toilet+paper", "&q=spring", "&q=love", "&q=baking", "&q=family"];
let type = "&image_type=photo";
let category = ["&category=emotions", "&category=science", "&category=health", "&category=health", "&category=nature", "&category=people", "&category=food", "&category=people"]


function setup() {
  createCanvas(windowWidth, windowHeight)
  background(180)

  //Text in the center of the canvas
  fill(0)
  textSize(30)
  textAlign(CENTER)
  textFont("Times New Roman")
  text("What are your thoughts on the current situation?", windowWidth/2, windowHeight/2)

  //"Negative" button
  buttonNegative = createButton("Negative");
  buttonNegative.size(100 ,100);
  buttonNegative.position(windowWidth/3-50, 30);
  buttonNegative.style("font-family", "Times New Roman")
  buttonNegative.style("font-weight", "bold")
  buttonNegative.style("font-size", "15px")
  buttonNegative.style("border-radius","50%");
  buttonNegative.style("background-color", "#000000")
  buttonNegative.style("color", "white");
  buttonNegative.mouseClicked(negative);

  //"Positive" button
  buttonPositive = createButton("Positive");
  buttonPositive.size(100 ,100);
  buttonPositive.position(2*windowWidth/3-50, 30);
  buttonPositive.style("font-family", "Times New Roman")
  buttonPositive.style("font-weight", "bold")
  buttonPositive.style("font-size", "15px")
  buttonPositive.style("border-radius","50%");
  buttonPositive.style("background-color", "#ffffff");
  buttonPositive.mouseClicked(positive);
}

//"Negative" URL link
function negative() {
  let num = int(random(0, 3))
  let url = api + apiKey + q[num] + type + category[num];
  loadJSON(url, gotData);
}

//"Positive" URL link
function positive() {
  let num = int(random(4, 7))
  let url = api + apiKey + q[num] + type + category[num];
  loadJSON(url, gotData);
}

//Displaying the images
function gotData(photo) {
  let img = createImg(photo.hits[int(random(20))].previewURL);
  img.position(random(0, windowWidth-170), random(150, windowHeight-130));
}
