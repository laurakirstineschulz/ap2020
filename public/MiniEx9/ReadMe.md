![Screenshot](Screenshot-MiniEx9.png)

**This Mini Ex is made in collaboration with Herborg Hjartvardsdóttir Kjærbæk, Line Stampe-Degn Møller and Linnea Aandahl**

**RUNME:** https://laurakirstineschulz.gitlab.io/ap2020/MiniEx9


**OUR VISION**
To begin with, our vision was to make a program that had the ability to generate art, inspired by the net.art-generator.com (NAG). The idea was that our program would generate art, not from searching words, but from using color codes. In trying to create our code we had a lot of issues with adding effects to the images, especially applying a gradient to the images (in order for them to be put on top of each other). The reason for this is that the images we were using are dom elements. Therefore we can not use an alpha value - the same way as in the syntax “fill”, where you fill in the values for RGB and add a fourth value (alpha value), which will determine the gradient.
These issues made us change our original vision. When generating ideas for this mini ex, coronavirus (COVID-19) came up several times. Therefore we decided to go with this theme instead. Our lives have been turned upside down by this pandemic - both personally and professionally. All of a sudden we are not allowed to see our friends or family. A lot of people have lost their jobs, or been sent home to receive the country's aid-packages. It is an overwhelming situation, with a lot of uncertainties. Therefore, it is becoming hard to visualize the situation, as well as one's thoughts and feelings.


**THE CODE**
When entering our program, you are met by one question: “What are your thoughts on the current situation?”. Also, there are the two buttons: One black button with the word “negative”, and one white button with the word “positive”. when clicking on one of these buttons an image will appear on the canvas. If you keep clicking either of these buttons, eventually the images will fill up the entire canvass. The images are generated based on search words and categories.

The search words are defined  in the sketch.js file as: `let q = ["&q=depression", "&q=virus", "&q=death", "&q=toilet+paper", "&q=spring", "&q=love","&q=baking", "&q=family"];`

The categories are defined as: `let category = ["&category=emotions", "&category=science", "&category=health", "&category=health", "&category=nature", "&category=people", "&category=food", "&category=people"]`

The above are the four search words and four categories for each button. These words/categories are based on things that have become evident and important in these pandemic times. When pressing a button and thereby displaying a images, the images are positioned at a random position within these parameter: `img.position(random(0, windowWidth-170), random(150, windowHeight-130))`. This is to make sure that the images won’t eventually cover the buttons. Also, we have determined the size of the images in order for them to keep the same size format, but also to enable the users to create their own artwork through a photo collage. To size the images, we have used the response key “previewURL” which is low resolution images with a maximum width or height of 150 pixels.


**REFLECTIONS ON APIs**
APIs make it possible to share and use data across different websites. In the text: “API practices and paradigms: Exploring the protocological parameters of API´s as key facilitators of sociotechnical forms of exchange”, the authors, Eric Snodgrass and Winnie Soon explains how the distribution of data through an API is controlled by the provider. This is something we also experienced in trying to find our API. The text mentions Google as a specific example of a dominating provider. According to the text, Google is one of many providers that charges those who request data. Figure 7 (Snodgrass, Eric, & Soon, Winnie) gives an overview of different limitations and charges on Google’s API services:

![Screenshot](Figure7.png)

The above makes data seem like an object with a high market value. One could argue that this generates an asymmetric relationship because it creates inequality between the user and the provider:

> “Web APIs can be understood as involving asymmetric forms of protocological exchange.” (Snodgrass, Eric, & Soon, Winnie).

We, as users, are the ones making/giving the data, but what do we get in return? We cannot give a concrete answer as data is difficult to put a price on. Also, we, as users, do not know exactly what we are giving/getting back, and therefore we do not know if we pay a fair price.

At last we want to mention that there is a key difference between Google images and Pixabay (the API  we have been using). Pixabay is a platform for sharing copyright free images and videos meaning that they are shared under the Pixabay License. This makes them free to use, without asking for permission or giving credit - both for personal and commercial use. This is why we made a conscious choice not to use a Google API, but use Pixabay instead. Pixabay has a downside though: We know that we won’t be stealing anyone's work, without knowing it (even if one should be aware of this). In regards to google, this could result in artists losing a lot of money, and also people copying the artists ideas.


**REFLECTIONS ON OUR PROGRAM**
The purpose of our program is to articulate the emotions associated with COVID-19. However, it is worth noting that our program only allows two emotions: positive or/and negative. This is a very deliberate choice, made in an attempt to illustrate the binary codes a computer contains. Binary is a numeric system that only uses two digits - 0 and 1. Computers operate in binary, meaning they store data and perform calculations using only zeros and ones. This two-symbol system is what we aim to show in our program by having only two options of emotions to choose from. And just like a binary computer system, the user cannot choose something outside or in between the options they have been given by the program.

The above derives from the fact that a computer is very limited on the behalf of human beings: We as humans are the ones who create as well as program these computers. This means, we are the ones making the decisions and therefore we are the ones having the ability to modify the world. From this week's learning, we know that it is often the larger companies (like Google or Facebook) that are in charge when it comes to the big decisions and modifications - just like they have charge over APIs. In Femke Snelting lecture called “Modifying the Universal”, Snelting is talking about how these companies are modifying something that in fact should be universal. The significance of this is that there always will be complications and further modifications to make. An example of both Snelting’s thinking and a binary system is Facebook. Facebook gives the user only six different ways to react on a post. This controls how the user interacts and communicates on Facebook, which is very much controlled by the developers.
Regardless, we still use Facebook.

In this pandemic era of COVID-19, social media such as facebook has become indispensable in order to communicate with our friends and family. And not just Facebook, but also media like Skype, Blackboard Collaborate, Zoom and so on. Right now, we are in a time where the computer's many ways of interacting are not just a luxury, but a necessity to make everyday life work. This pandemic has meant that our computer suddenly contains both school, work, friends and family.

As mentioned in the OUR CODE section, in order for the program to search images we have selected a number of keywords that we associate with COVID-19. However, these keywords can be very individual. Instead of “spring”, “autumn” could have been written. Also, we have put family in relation with the positive button. This is not necessarily the same for everyone as many are not allowed to visit their family and for that reason the noun becomes negative. Therefore, the positive and negative images of this program could be looked at in very different ways.


**IF WE HAD MORE TIME**
We would like to know, how is the data collected? And are there any rules regarding collecting and making APIs public? Also, we have talked quite a lot about how the API chooses which picture to display - is it completely random, or is there a patton?

**SOURCES**
* API: https://pixabay.com/api/docs/
* Video: Femke Snelting, “Modifying the Universal”, MedeaTV, (2016).
* Text: Snodgrass, Eric, & Soon, Winnie. "API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange." First Monday, (2019): Web. 4 April. 2020
