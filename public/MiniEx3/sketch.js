function setup() {
  createCanvas(windowWidth, windowHeight); //Filling in the whole canvas
  frameRate(10);  //The number of frames to be displayed per second
}

function draw() {
  fill(225, 225, 208, 50); //Filling in the background
  rect(0, 0, width, height);
  drawElements1();//THE INNER THROBBER
  drawElements2();//THE MIDDEL THROBBER
  drawElements3();//THE OUTER THROBBER
}


//THE INNER THROBBER
function drawElements1() {
  let num = 15; //The number of ellipses within the throbber
  push();
  translate(width/2, height/2); //Moves the throbber to the center

 //indicates the movement of the throbber
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));

  noStroke(); //The stroke
  fill(78, 78, 50); //Changes the color of the throbber
  ellipse(50,0,50,10);  //Changes the shape of the throbber
  pop();
}


//THE MIDDEL THROBBER
function drawElements2() {
  let num = 15; //The number of ellipses within the throbber
  push();
  translate(width/2, height/2); //Moves the throbber to the center

 //indicates the movement of the throbber
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));

  noStroke(); //The stroke
  fill(124, 124, 80); //Changes the color of the throbber
  ellipse(150,0,70,20);  //Changes the shape of the throbber
  pop();
}


//THE OUTER THROBBER
function drawElements3() {
  let num = 15; //The number of ellipses within the throbber
  push();
  translate(width/2, height/2); //Moves the throbber to the center

 //indicates the movement of the throbber
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));

  noStroke(); //The stroke
  fill(185, 185, 146); //Changes the color of the throbber
  ellipse(250,0,90,20);  //Changes the shape of the throbber
  pop();
}


//Automatically adjusts the height and width whenever the size of the window is increased
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
