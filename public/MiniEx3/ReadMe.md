LINK FOR MY SKETCH: https://laurakirstineschulz.gitlab.io/ap2020/MiniEx3/



**MY SKETCH**

My sketch is, no surprise, a throbber.

In this mini ex I have worked with the function *drawElements*. My vision was to create a fairly simple throbber, based on one of Winnie's codes (http://siusoon.gitlab.io/aesthetic-programming/sketch03/). Winnie worked with only one element, but I decided to add two additional elements in trying to create coherent throbber, with more that just one dimension. My main focus circled around on the throbber’s form and its colors - I wanted to make something that looked aesthetically neat. In the trying to make an ombre effect I used different colorcodes. Afterwards I realized that I just af well could have used the alpha effect that p5.js supports. In my sketch.js, I have added quite a lot of //notes, so my approach should be easily read from there.


**TIME AND ITS MEANING**

In my function setup I used *frameRate(10)*. FrameRate specifies the number of frames to be displayed every second. The default rate is 60 frames per second but by setting my frameRate to 10, my program will show only 10 frames every sekund and therefor my throbber will run slower.

A throbber represents the speed of network traffic which is also tied to our affective states and perception of time. Therefor the throbber icon acts as an interface between computational processes and visual communication. If I had not used the syntax: frameRate(10), and just kept the default rate, maybe the waiting time, that a throbber in many ways symbolizes, would seem faster. This because the network would seem to run better, and one would not sit with the feeling of wasted and unproductive time.